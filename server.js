// Require packages and set the port
const routes = require('./routes/routes');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
var dialogflow = require('dialogflow');

const port = 8080;
var sessionClient = new dialogflow.SessionsClient();
var languageCode = 'en-US'
const sessionPath = sessionClient.sessionPath('newagent-rifpgt', "123456".toString());
json_holder_yes = null

sessionArray = {}

// Use Node.js body parsing middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));

// routes(app)

async function dectectIntent(sessionPath, query, contexts, languageCode) {
    // The text query request.
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          text: query,
          languageCode: languageCode,
        },
      },
    };
  
    if (contexts && contexts.length > 0) {
      request.queryParams = {
        contexts: contexts,
      };
    }
  
    const responses = await sessionClient.detectIntent(request);
    return responses[0];
}

// enabling CORS
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  next();
});

app.post('/intent', async (req, res) => {
    if(!req.body.message && !req.body.context) {
      return res.status(400).send({
        success: 'false',
        message: 'message not found'
      })
    }

    context = JSON.parse(req.body.context)

    // console.log(Object.keys(context).length)
    // console.log(req.body.context)

    if (Object.keys(context).length > 0) {
      result = await dectectIntent(sessionPath, req.body.message, context.outputContexts, languageCode);
      // console.log("yes")
      // console.log(context.outputContexts)
    }
    else {
      result = await dectectIntent(sessionPath, req.body.message, null, languageCode);
      // console.log("no")
    }
    
    // console.log(result['queryResult']['fulfillmentText'])

    return res.status(201).send({
        message: result['queryResult']
    })
})

app.get('/chat', async function(req, res) {
  var message = req.query.text

  sessId = req.query.sessionId;

  if ((sessId in sessionArray) == false) {
    sessionArray[sessId] = {
      "context_holder": null
    }
  }

  const response = {
    output: []
  };

  const msg = response.output;

  if (sessionArray[sessId]["context_holder"] == null) {

    result = await dectectIntent(sessionClient.sessionPath('newagent-rifpgt', sessId), message, sessionArray[sessId]["context_holder"], languageCode);
    sessionArray[sessId]["context_holder"] = result['queryResult']["outputContexts"]
    mess_response = result['queryResult']['fulfillmentMessages'][0]['text']['text'][0]
    custom_response = result['queryResult']['fulfillmentMessages'][1]['payload']['fields']
    mess_custom = custom_response['message']['stringValue']

    quickReplies = custom_response['metadata']['structValue']['fields']['payload']['listValue']['values']

    var opts = []
    for (i = 0; i < quickReplies.length; i++) {
      quickRep = quickReplies[i]['structValue']['fields']['name']['stringValue']
      opts.push({label: quickRep, value: quickRep});
    }

    msg.push({
      type: 'text',
      value: `${mess_response}`
    });

    msg.push({
      type: 'text',
      value: `${mess_custom}`
    });

    msg.push({
      type: "option",
      options: opts
    });
    

  }
  else {

    if (message == "Yes") {
      result = await dectectIntent(sessionClient.sessionPath('newagent-rifpgt', sessId), message, json_holder_yes, languageCode);      
      custom_response = result['queryResult']['fulfillmentMessages'][1]['payload']['fields']
      mess_custom = custom_response['message']['stringValue']

      quickReplies = custom_response['metadata']['structValue']['fields']['payload']['listValue']['values']

      var opts = []
      for (i = 0; i < quickReplies.length; i++) {
        quickRep = quickReplies[i]['structValue']['fields']['name']['stringValue']
        opts.push({label: quickRep, value: quickRep});
      }

      msg.push({
        type: 'text',
        value: `${mess_custom}`
      });

      msg.push({
        type: "option",
        options: opts
      });
    }
    else if (message == "No") {
      sessionArray[sessId]["context_holder"] = null
      msg.push({
        type: 'text',
        value: "Goodbye!"
      });
    }
    else if (message.toString() == "1" || message.toString() == "2" || message.toString() == "3" || message.toString() == "4" || message.toString() == "5") {
      sessionArray[sessId]["context_holder"] = null
      json_holder_yes = null
      result = await dectectIntent(sessionClient.sessionPath('newagent-rifpgt', sessId), message, sessionArray[sessId]["context_holder"], languageCode);
      json_holder_yes = result['queryResult']["outputContexts"]
      sessionArray[sessId]["context_holder"] = null
      mess_response = result['queryResult']['fulfillmentMessages'][0]['text']['text'][0]

      msg.push({
        type: 'text',
        value: `${mess_response}`
      });

      if (sessId in sessionArray) {
        delete sessionArray[sessId]
      }
    }
    else {
      result = await dectectIntent(sessionClient.sessionPath('newagent-rifpgt', sessId), message, sessionArray[sessId]["context_holder"], languageCode);
      json_holder_yes = result['queryResult']["outputContexts"]
      sessionArray[sessId]["context_holder"] = null
      mess_response = result['queryResult']['fulfillmentMessages'][0]['text']['text'][0]

      msg.push({
        type: 'text',
        value: `${mess_response}`
      });

      // Option 1
      if (message == "Las Vegas") {

        msg.push({
            type: 'html',
            value: 'Click <a href="https://en.wikipedia.org/wiki/Las_Vegas" target="_blank" >here</a> for more infomation'
        });

        msg.push({
          type: 'image',
          value: 'http://lasvegas.net/uploads/home_pages/home-banner.jpg'
        })
      }
      // Option 2
      else if (message == "Transportation") {
 
        msg.push({
          type: 'html',
          value: 'Click <a href="https://www.google.com/maps/place/Las+Vegas,+NV,+USA/@36.1246738,-115.4551869,10z/data=!3m1!4b1!4m5!3m4!1s0x80beb782a4f57dd1:0x3accd5e6d5b379a3!8m2!3d36.1699412!4d-115.1398296" target="_blank" >here</a> for more information.'
        })
      }
      //Option 3
      else if (message == "Hotels"){
 
        msg.push({
          type: 'html',
          value: 'Click <a href="http://hotels.lasvegas.net/cities/8654-800049030" target="_blank" >here</a>  for more information.'
        })
      }
      // Option 4
      else if (message == "Casino"){

        msg.push({
          type: 'html',
          value: 'Click <a href="http://lasvegas.net/casinos" target="_blank" >here</a>  for more information.'
        })
      }
      // Option 5
      else if (message == "Shows"){

        msg.push({
          type: 'html',
          value: 'Click <a href="http://lasvegas.net/shows" target="_blank" >here</a>  for more information.'
        })
      }
      // Option 6
      else if (message == "Sports"){
 
        msg.push({
          type: 'html',
          value: 'Click <a href="http://lasvegas.net/sports" target="_blank" >here</a>  for more information.'
        })
      }
      // Option 7
      else if (message == "Nightlife"){
 
        msg.push({
          type: 'html',
          value: 'Click <a href="http://lasvegas.net/nightlife" target="_blank" >here</a>  for more information.'
        })
      }
      ///////////////////////////
      msg.push({
        type: 'text',
        value: "Would you like to know more?"
      });

      var opts = []
      opts.push({label: "Yes", value: "Yes"});
      opts.push({label: "No", value: "No"});
      msg.push({
        type: "option",
        options: opts
      });
      
    }
  }

  res.json(response);
})

// Start the server
const server = app.listen(port, (error) => {
    if (error) return console.log(`Error: ${error}`);
 
    console.log(`Server listening on port ${server.address().port}`);
});